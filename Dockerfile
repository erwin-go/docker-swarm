FROM golang:1.22.2-alpine AS builder

WORKDIR /app

COPY . .

RUN go mod tidy && go mod download

ENV CGO_ENABLED=0 GOOS=linux GOARCH=amd64

RUN go build -o go-docker-swarm .

FROM scratch

COPY --from=builder /app/go-docker-swarm /

CMD [ "./go-docker-swarm" ]

